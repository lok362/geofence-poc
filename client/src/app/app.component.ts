import { Component, HostListener } from "@angular/core";
import { Store } from "@ngrx/store";
import { take } from "rxjs/operators";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
})
export class AppComponent {

  constructor(
    private readonly store: Store<any>,
  ) {}

  @HostListener("window:keyup", ["$event"])
  public onWindowKeyUp(e: KeyboardEvent) {
    if (e.shiftKey && e.key === "S") { // Shift + S
      this.store.pipe(take(1)).subscribe(console.log);
    }
  }

}
