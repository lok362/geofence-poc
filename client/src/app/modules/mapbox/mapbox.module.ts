import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MapboxComponent } from "./components/mapbox/mapbox.component";
import { StoreModule } from "@ngrx/store";
import { featureEntityReducer } from "./store/feature/feature.reducer";
import { editEntityReducer } from "./store/edit/edit.reducer";

@NgModule({
  declarations: [
    MapboxComponent,
  ],
  imports: [
    CommonModule,
    StoreModule.forFeature("mapbox", {
      feature: featureEntityReducer,
      edit: editEntityReducer,
    }),
  ],
  exports: [
    MapboxComponent,
  ],
})
export class MapboxModule { }
