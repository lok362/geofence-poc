import { FeatureState } from "./feature/feature.reducer";
import { EditState } from "./edit/edit.reducer";

export interface MapboxModuleState {
  feature: FeatureState;
  edit: EditState;
}
