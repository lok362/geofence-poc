import * as GeoJson from "geojson";

export type FeatureEntity = GeoJson.Feature & { id: string };
