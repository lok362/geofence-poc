import { createReducer, on } from "@ngrx/store";
import { EntityState, createEntityAdapter } from "@ngrx/entity";
import { FeatureEntity } from "./feature.entity";
import * as FeatureActions from "./feature.actions";
import * as faker from "faker";

export type FeatureState = EntityState<FeatureEntity>;

export const adapter = createEntityAdapter<FeatureEntity>();

const initialState = adapter.getInitialState();

export const featureEntityReducer = createReducer(
  initialState,
  on(FeatureActions.add, (state, { features }) => {
    return adapter.addMany(features, state);
  }),
  on(FeatureActions.update, (state, { features }) => {
    return adapter.updateMany(
      features.map(f => ({ id: f.id , changes: f })),
      state,
    );
  }),
  on(FeatureActions.remove, (state, { featureIds }) => {
    return adapter.removeMany(featureIds, state);
  }),
  on(FeatureActions.clear, state => {
    return adapter.removeAll(state);
  }),
  on(FeatureActions.upsert, (state, { features }) => {
    for (const feature of features) {
      if ((state.ids as string[]).indexOf(feature.id) === -1) {
        if (feature.geometry.type === "Point") {
          feature.properties.name = `${faker.name.firstName()} ${faker.name.lastName()}`;
        } else if (feature.geometry.type === "Polygon") {
          feature.properties.name = faker.company.companyName();
        }
      }
    }
    return adapter.upsertMany(features, state);
  }),
);
