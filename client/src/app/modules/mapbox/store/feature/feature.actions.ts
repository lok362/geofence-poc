import { createAction, props } from "@ngrx/store";
import { FeatureEntity } from "./feature.entity";

export const add = createAction("[Feature] Add", props<{ features: FeatureEntity[] }>());
export const update = createAction("[Feature] Update", props<{ features: (Partial<FeatureEntity> & { id: string })[] }>());
export const remove = createAction("[Feature] Remove", props<{ featureIds: string[] }>());
export const clear = createAction("[Feature] Clear");
export const upsert = createAction("[Feature] Upsert", props<{ features: FeatureEntity[] }>());
