import { MapboxModuleState } from "..";
import { adapter } from "./feature.reducer";
import { createSelector } from "@ngrx/store";

const selectFeatureState = (state: { mapbox: MapboxModuleState }) => state.mapbox.feature;

const {
  selectIds,
  selectAll,
  selectTotal,
} = adapter.getSelectors();

export const ids = createSelector(selectFeatureState, selectIds);

export const list = createSelector(selectFeatureState, selectAll);

export const total = createSelector(selectFeatureState, selectTotal);

export const polygonList = createSelector(list, features => features.filter(f => f.geometry.type === "Polygon"));

export const pointList = createSelector(list, features => features.filter(f => f.geometry.type === "Point"));

export const featureCollection = createSelector(
  list,
  (features) => ({ type: "FeatureCollection" as const, features }),
);
