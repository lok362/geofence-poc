import { MapboxModuleState } from "..";
import { adapter } from "./edit.reducer";
import { createSelector } from "@ngrx/store";

const selectEditState = (state: { mapbox: MapboxModuleState }) => state.mapbox.edit;

const {
  selectIds,
  selectAll,
  selectTotal,
} = adapter.getSelectors();

export const ids = createSelector(selectEditState, selectIds);

export const list = createSelector(selectEditState, selectAll);

export const total = createSelector(selectEditState, selectTotal);
