import { createAction, props } from "@ngrx/store";
import { FeatureEntity } from "../feature/feature.entity";

export const create = createAction("[Edit] Create", props<{ feature: FeatureEntity }>());
export const upsert = createAction("[Edit] Upsert", props<{ features: FeatureEntity[] }>());
export const remove = createAction("[Edit] Remove", props<{ featureIds: string[] }>());
export const clear = createAction("[Edit] Clear");
