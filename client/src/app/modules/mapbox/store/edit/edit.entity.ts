import { FeatureEntity } from "../feature/feature.entity";

export type EditEntity = FeatureEntity & { id: string };
