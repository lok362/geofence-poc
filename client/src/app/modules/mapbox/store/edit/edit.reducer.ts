import { EntityState, createEntityAdapter } from "@ngrx/entity";
import { EditEntity } from "./edit.entity";
import { createReducer, on } from "@ngrx/store";
import * as EditActions from "./edit.actions";

export type EditState = EntityState<EditEntity>;

export const adapter = createEntityAdapter<EditEntity>();

const initialState = adapter.getInitialState();

export const editEntityReducer = createReducer(
  initialState,
  on(EditActions.create, (state, { feature }) => {
    return adapter.addOne({ ...feature, properties: { created: Date.now() }}, state);
  }),
  on(EditActions.upsert, (state, { features }) => {
    return adapter.upsertMany(features, state);
  }),
  on(EditActions.remove, (state, { featureIds }) => {
    return adapter.removeMany(featureIds, state);
  }),
  on(EditActions.clear, (state) => {
    return adapter.removeAll(state);
  }),
);
