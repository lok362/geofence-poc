import * as MapboxGl from "mapbox-gl";
import * as MapboxGlDraw from "@mapbox/mapbox-gl-draw/dist/mapbox-gl-draw";
import { Subject } from "rxjs";
import * as GeoJson from "geojson";

export type MapDrawHelperFeature = GeoJson.Feature & { id: string };

export class MapDrawHelper {

  public onCreate = new Subject<{ features: MapDrawHelperFeature[] }>();
  public onUpdate = new Subject<{ features: MapDrawHelperFeature[] }>();
  public onDelete = new Subject<{ features: MapDrawHelperFeature[] }>();

  public readonly control: MapboxGlDraw;

  constructor(
    private readonly map: MapboxGl.Map,
  ) {
    this.control = new MapboxGlDraw({
      displayControlsDefault: false,
      controls: {
       point: true,
       polygon: true,
       trash: true,
      },
    });
  }

  public show() {
    this.map.addControl(this.control);
    this.map.on("draw.create", e => this.onCreate.next(e));
    this.map.on("draw.update", e => this.onUpdate.next(e));
    this.map.on("draw.delete", e => this.onDelete.next(e));
  }

  public hide() {
    this.map.removeControl(this.control);
  }

}
