import { Observable, BehaviorSubject, forkJoin, of } from "rxjs";
import { distinctUntilChanged, map, switchMap } from "rxjs/operators";

export class MapModeHelper<T extends {
  [k: string]: {
    canEnter?: () => Observable<boolean>|Promise<boolean>|boolean;
    onEnter?: () => void;
    canExit?: () => Observable<boolean>|Promise<boolean>|boolean;
    onExit?: () => void;
  };
}> {

  public value$: Observable<keyof T>;

  private valueSubject: BehaviorSubject<keyof T>;

  constructor(private readonly modes: T, private readonly opts: { initialMode: keyof T }) {
    this.valueSubject = new BehaviorSubject(this.opts.initialMode);
    this.value$ = this.valueSubject.pipe(distinctUntilChanged());
    const onEnter = this.modes[this.opts.initialMode].onEnter;
    if (onEnter) { onEnter(); }
  }

  public set(mode: keyof T): void {
    const prevMode = this.valueSubject.value;
    const canExitCheck = this.modes[prevMode]?.canExit;
    const canEnterCheck = this.modes[mode]?.canEnter;
    forkJoin({
      curr: of(mode),
      prev: of(prevMode),
      canExit: canExitCheck ? canExitCheck() : of(true),
      canEnter: canEnterCheck ? canEnterCheck() : of(true),
    }).pipe(
      map(({ curr, prev, canExit, canEnter }) => {
        if (!canExit || !canEnter) {
          return;
        }
        const onExit = this.modes[prev]?.onExit;
        if (onExit) { onExit(); }
        const onEnter = this.modes[curr]?.onEnter;
        if (onEnter) { onEnter(); }

        this.valueSubject.next(mode);
      }),
    ).subscribe();
  }

  public get(): keyof T {
    return this.valueSubject.value;
  }

}
