import { Component, ViewChild, ElementRef, AfterViewInit, OnDestroy } from "@angular/core";
import { environment } from "src/environments/environment";
import * as MapboxGl from "mapbox-gl";
import { Store, select } from "@ngrx/store";
import { take, map, switchMap, tap, catchError } from "rxjs/operators";
import { MapDrawHelper } from "../../helpers/map-draw-helper";
import { MapModeHelper } from "../../helpers/map-mode-helper";
import { MapboxModuleState } from "../../store";
import * as FeatureEntityActions from "../../store/feature/feature.actions";
import * as FeatureEntitySelectors from "../../store/feature/feature.selectors";
import * as EditEntityActions from "../../store/edit/edit.actions";
import * as EditEntitySelectors from "../../store/edit/edit.selectors";
import { Subject, of, Observable, forkJoin, throwError } from "rxjs";
import { ApiService } from "src/app/api.service";

@Component({
  selector: "app-mapbox",
  templateUrl: "./mapbox.component.html",
  styleUrls: ["./mapbox.component.scss"],
})
export class MapboxComponent implements AfterViewInit, OnDestroy {

  public isLoading = true;
  public loadingMessage = "Loading...";

  public unsavedChanges = false;

  public showEditModal = false;
  public editModalMessage = "";
  public editModalSubject = new Subject<"apply"|"revert"|"cancel">();

  public mode = new MapModeHelper({
    view: {
      onEnter: () => {
        if (!this.isLoading) {
          this.updateView();
        }
      },
      onExit: () => {
        if (!this.isLoading) {
          this.clearView();
        }
      },
    },
    edit: {
      onEnter: () => {
        this.mapDraw.show();
        this.store.pipe(
          take(1),
          select(FeatureEntitySelectors.featureCollection),
        ).subscribe(fc => {
          this.mapDraw.control.set(fc);
        });
      },
      canExit: () => {
        return this.store.pipe(
          take(1),
          select(EditEntitySelectors.total),
          switchMap(num => {
            if (num === 0) {
              return of(true);
            } else {
              this.showEditModal = true;
              this.editModalMessage = `You've made ${num} change${num > 1 ? "s" : ""}`;
              return this.editModalSubject.pipe(
                take(1),
                switchMap(choice => {
                  if (choice === "apply") {
                    return this.store.pipe(
                      take(1),
                      select(EditEntitySelectors.list),
                      map(edits => {
                        this.store.dispatch(FeatureEntityActions.upsert({ features: edits }));
                        this.store.dispatch(EditEntityActions.clear());
                        this.unsavedChanges = true;
                        return true;
                      }),
                    );
                  } else if (choice === "revert") {
                    this.store.dispatch(EditEntityActions.clear());
                    return of(true);
                  } else {
                    return of(false);
                  }
                }),
              );
            }
          }),
        );
      },
      onExit: () => {
        this.mapDraw.hide();
      },
    },
  }, { initialMode: "view" });

  @ViewChild("mapContainer") private readonly mapContainer: ElementRef<HTMLDivElement>;

  private map: MapboxGl.Map;
  private mapDraw: MapDrawHelper;
  private markers: MapboxGl.Marker[] = [];

  constructor(
    private readonly store: Store<{ mapbox: MapboxModuleState }>,
    private readonly apiService: ApiService,
  ) {}

  public ngAfterViewInit() {
    forkJoin(
      this.initMap(),
      this.apiService.getFeatures(),
    ).subscribe(() => {
      this.mode.set("view");
      this.initEdit();
    });
  }

  public ngOnDestroy() {
    this.map.remove();
  }

  public makeEditModalChoice(choice: "apply"|"revert"|"cancel") {
    this.showEditModal = false;
    this.editModalSubject.next(choice);
  }

  public sync() {
    this.isLoading = true;
    this.loadingMessage = "Synchronizing changes...";
    this.apiService.synchronizeFeatures().subscribe(
      () => { this.isLoading = false; this.unsavedChanges = false; },
      err => console.error(err),
    );
  }

  public test() {
    this.isLoading = true;
    this.loadingMessage = "Testing...";
    this.store.pipe(
      take(1),
      switchMap(state => {
        return forkJoin({
          users: of(FeatureEntitySelectors.pointList(state)),
          areas: of(FeatureEntitySelectors.polygonList(state)),
        });
      }),
      switchMap(({ users, areas }) => {
        const reqMap: any = {};
        for (const user of users) {
          const userName = user.properties.name;
          reqMap[userName] = forkJoin({
            areasInside: this.apiService.getAreasUserIsWithin(userName),
          });
        }
        for (const area of areas) {
          const areaName = area.properties.name;
          reqMap[areaName] = forkJoin({
            usersWithin: this.apiService.getUsersWithinArea(areaName),
            usersOutside: this.apiService.getUsersOutsideArea(areaName),
          });
        }
        return forkJoin(reqMap);
      }),
      tap(() => { this.isLoading = false; }),
    ).subscribe(
      res => console.log(res),
      err => console.error(err),
    );
  }

  private initMap(): Observable<void> {
    this.isLoading = true;
    this.map = new MapboxGl.Map({
      accessToken: environment.mapbox.accessToken,
      style: "mapbox://styles/mapbox/dark-v10",
      container: this.mapContainer.nativeElement,
      center: [10.748854, 59.9193027],
      zoom: 12,
    });
    return new Observable(obs => {
      this.map.once("load", () => {
        this.isLoading = false;

        this.map.on("click", "areas", e => {
          new MapboxGl.Popup({ closeButton: false })
            .setLngLat(e.lngLat)
            .setHTML(`<strong>${e.features[0].properties.name}</strong>`)
            .addTo(this.map);
        });

        obs.next();
        obs.complete();
      });
    });
  }

  private initEdit() {
    this.mapDraw = new MapDrawHelper(this.map);
    this.mapDraw.onCreate.subscribe(e => this.store.dispatch(EditEntityActions.create({ feature: e.features[0] })));
    this.mapDraw.onUpdate.subscribe(e => this.store.dispatch(EditEntityActions.upsert(e)));
    this.mapDraw.onDelete.subscribe(e => this.store.dispatch(EditEntityActions.remove({ featureIds: e.features.map(f => f.id) })));
  }

  private updateView() {
    this.store.pipe(
      take(1),
      switchMap(state => {
        return forkJoin({
          users: of(FeatureEntitySelectors.pointList(state)),
          areas: of(FeatureEntitySelectors.polygonList(state)),
        });
      }),
    ).subscribe(({ users, areas }) => {
      const layerId = "areas";
      this.map.addSource(layerId, {
        type: "geojson",
        data: {
          type: "FeatureCollection",
          features: areas,
        },
      });
      this.map.addLayer({
        id: layerId,
        type: "fill",
        source: layerId,
        layout: {},
        paint: {
          "fill-color": "#088",
          "fill-opacity": 0.8,
        },
      });

      for (const user of users) {
        const m = new MapboxGl.Marker()
          .setLngLat((user.geometry as any).coordinates)
          .setPopup(
            new MapboxGl.Popup({
              closeButton: false,
            })
            .setHTML(`<strong>${user.properties.name}</strong>`),
          )
          .addTo(this.map);
        this.markers.push(m);
      }
    });
  }

  private clearView() {
    const layerId = "areas";
    if (this.map.getLayer(layerId)) {
      this.map.removeLayer(layerId).removeSource(layerId);
    }
    if (this.markers) {
      for (const marker of this.markers) {
        marker.remove();
      }
      this.markers = [];
    }
  }

}
