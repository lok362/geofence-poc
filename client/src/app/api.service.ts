import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Store, select } from "@ngrx/store";
import { MapboxModuleState } from "./modules/mapbox/store";
import { forkJoin, of, Observable } from "rxjs";
import { switchMap, take, map } from "rxjs/operators";
import * as FeatureEntitySelectors from "./modules/mapbox/store/feature/feature.selectors";
import * as GeoJson from "geojson";
import * as FeatureEntityActions from "./modules/mapbox/store/feature/feature.actions";
import { FeatureEntity } from "./modules/mapbox/store/feature/feature.entity";

@Injectable({ providedIn: "root" })
export class ApiService {

  private readonly baseUrl = "http://localhost:4200/api";

  constructor(
    private readonly http: HttpClient,
    private readonly store: Store<{ mapbox: MapboxModuleState }>,
  ) {}

  public getUsersWithinArea(areaName: string): Observable<{ id: string, name: string }[]> {
    return this.http.get<{ id: string, name: string }[]>(this.baseUrl + "/geofence/users/within/" + areaName);
  }

  public getUsersOutsideArea(areaName: string): Observable<{ id: string, name: string }[]> {
    return this.http.get<{ id: string, name: string }[]>(this.baseUrl + "/geofence/users/outside/" + areaName);
  }

  public getAreasUserIsWithin(userName: string): Observable<{ id: string, name: string }[]> {
    return this.http.get<{ id: string, name: string }[]>(this.baseUrl + "/geofence/areas/containing/" + userName);
  }

  public getFeatures(): Observable<void> {
    return forkJoin({
      users: this.http.get<{ id: string, name: string, geometry: GeoJson.Point }[]>(this.baseUrl + "/users"),
      areas: this.http.get<{ id: string, name: string, geometry: GeoJson.Polygon }[]>(this.baseUrl + "/areas"),
    }).pipe(
      map(({ users, areas }) => {
        return {
          users: users.map(u => ({
            type: "Feature",
            id: u.id,
            properties: { name: u.name },
            geometry: u.geometry,
          })) as FeatureEntity[],
          areas: areas.map(a => ({
            type: "Feature",
            id: a.id,
            properties: { name: a.name },
            geometry: a.geometry,
          })) as FeatureEntity[],
        };
      }),
      switchMap(({ users, areas }) => {
        this.store.dispatch(FeatureEntityActions.add({ features: [...users, ...areas] }));
        return of(undefined);
      }),
    );
  }

  public synchronizeFeatures(): Observable<void> {
    console.log("Starting \"synchronization\"");
    return this.http.delete<number>(this.baseUrl + "/users").pipe(
      switchMap(deletedUsers => {
        console.log(`Deleted ${deletedUsers} users`);
        return this.store.pipe(take(1), select(FeatureEntitySelectors.pointList));
      }),
      switchMap(userPoints => {
        const uploads: Observable<any>[] = [];
        for (const userPoint of userPoints) {
          const res = this.http.post(this.baseUrl + "/users", {
            name: userPoint.properties.name,
            geometry: userPoint.geometry,
          });
          uploads.push(res);
        }
        return forkJoin(uploads);
      }),
      switchMap(completedUserUploads => {
        console.log(`Uploaded ${completedUserUploads.length} users successfully`);
        return this.http.delete<number>(this.baseUrl + "/areas");
      }),
      switchMap(deletedAreas => {
        console.log(`Deleted ${deletedAreas} areas`);
        return this.store.pipe(take(1), select(FeatureEntitySelectors.polygonList));
      }),
      switchMap(areaPolygons => {
        const uploads: Observable<any>[] = [];
        for (const areaPolygon of areaPolygons) {
          const res = this.http.post(this.baseUrl + "/areas", {
            name: areaPolygon.properties.name,
            geometry: areaPolygon.geometry,
          });
          uploads.push(res);
        }
        return forkJoin(uploads);
      }),
      switchMap(completedAreaUploads => {
        console.log(`Uploaded ${completedAreaUploads.length} areas successfully`);
        console.log("Synchronization finished!");
        return of(undefined);
      }),
    );
  }

}
