import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { StoreModule } from "@ngrx/store";
import { HttpClientModule } from "@angular/common/http";

import { AppComponent } from "./app.component";
import { MapboxModule } from "./modules/mapbox/mapbox.module";

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    StoreModule.forRoot({}),
    MapboxModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule { }
