import { Controller, Get, Param, Patch, Body, Delete, Post } from "@nestjs/common";
import { AreaService } from "./area.service";
import { AreaDto, AreaPatchDto, AreaCreateDto } from "src/data/dtos";

@Controller("areas")
export class AreaController {

  constructor(
    private readonly areaService: AreaService,
  ) {}

  @Get()
  public getAreas(): Promise<AreaDto[]> {
    return this.areaService.getAreas();
  }

  @Get(":id")
  public getAreaById(@Param("id") id: string): Promise<AreaDto> {
    return this.areaService.getAreaById(id);
  }

  @Post()
  public createArea(@Body() body: AreaCreateDto): Promise<AreaDto> {
    return this.areaService.createArea(body);
  }

  @Patch(":id")
  public updateArea(@Param("id") id: string, @Body() body: AreaPatchDto): Promise<AreaDto> {
    return this.areaService.updateArea(id, body);
  }

  @Delete(":id")
  public deleteArea(@Param("id") id: string): Promise<AreaDto> {
    return this.areaService.deleteArea(id);
  }

  @Delete()
  public deleteAllAreas(): Promise<number> {
    return this.areaService.deleteAllAreas();
  }

}