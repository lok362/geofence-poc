import { Injectable, NotFoundException } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model, Types } from "mongoose";
import { Area } from "src/data/models";
import { AreaDto, AreaPatchDto, AreaCreateDto } from "src/data/dtos";
import { mapAreaEntityToAreaDto } from "src/data/mappers";

@Injectable()
export class AreaService {

  constructor(
    @InjectModel("Area") private readonly areaModel: Model<Area>,
  ) {}

  public async getAreas(): Promise<AreaDto[]> {
    const areas = await this.areaModel.find().exec();
    return areas.map(mapAreaEntityToAreaDto);
  }

  public async getAreaById(id: string): Promise<AreaDto> {
    const area = await this.areaModel.findById(Types.ObjectId(id)).exec();
    if (!area) {
      throw new NotFoundException(`Area with id ${id} does not exist.`);
    }
    return mapAreaEntityToAreaDto(area);
  }

  public async createArea(areaCreateDto: AreaCreateDto): Promise<AreaDto> {
    const area = await this.areaModel.create(areaCreateDto);
    return mapAreaEntityToAreaDto(area);
  }

  public async updateArea(id: string, update: AreaPatchDto): Promise<AreaDto> {
    const area = await this.areaModel.findByIdAndUpdate(Types.ObjectId(id), update);
    if (!area) {
      throw new NotFoundException(`Area with id ${id} does not exist.`);
    }
    return mapAreaEntityToAreaDto(area);
  }

  public async deleteArea(id: string): Promise<AreaDto> {
    const area = await this.areaModel.findByIdAndDelete(Types.ObjectId(id)).exec();
    if (!area) {
      throw new NotFoundException(`Area with id ${id} does not exist.`);
    }
    return mapAreaEntityToAreaDto(area);
  }

  public async deleteAllAreas(): Promise<number> {
    const res = await this.areaModel.deleteMany({}).exec();
    return res.deletedCount;
  }

}