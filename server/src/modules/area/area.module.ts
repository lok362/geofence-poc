import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { AreaSchema } from "src/data/schemas";
import { AreaService } from "./area.service";
import { AreaController } from "./area.controller";

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: "Area", schema: AreaSchema },
    ]),
  ],
  controllers: [
    AreaController,
  ],
  providers: [
    AreaService,
  ],
})
export class AreaModule {

}