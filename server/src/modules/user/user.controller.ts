import { Controller, Get, Param, Delete, Patch, Body, Post } from "@nestjs/common";
import { UserService } from "./user.service";
import { UserDto, UserPatchDto, UserCreateDto } from "src/data/dtos";

@Controller("users")
export class UserController {

  constructor(
    private readonly userService: UserService,
  ) {}

  @Get()
  public getUsers(): Promise<UserDto[]> {
    return this.userService.getUsers();
  }

  @Get(":id")
  public getUserById(@Param("id") id: string): Promise<UserDto> {
    return this.userService.getUserById(id);
  }

  @Post()
  public createUser(@Body() body: UserCreateDto): Promise<UserDto> {
    return this.userService.createUser(body);
  }

  @Patch(":id")
  public updateUser(@Param("id") id: string, @Body() body: UserPatchDto): Promise<UserDto> {
    return this.userService.updateUser(id, body);
  }

  @Delete(":id")
  public deleteUser(@Param("id") id: string): Promise<UserDto> {
    return this.userService.deleteUser(id);
  }

  @Delete()
  public deleteAllUsers(): Promise<number> {
    return this.userService.deleteAllUsers();
  }

}