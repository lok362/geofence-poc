import { Injectable, NotFoundException } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model, Types } from "mongoose";
import { User } from "src/data/models";
import { UserDto, UserPatchDto, UserCreateDto } from "src/data/dtos";
import { mapUserEntityToUserDto } from "src/data/mappers";

@Injectable()
export class UserService {

  constructor(
    @InjectModel("User") private readonly userModel: Model<User>,
  ) {}

  public async getUsers(): Promise<UserDto[]> {
    const users = await this.userModel.find().exec();
    return users.map(mapUserEntityToUserDto);
  }

  public async getUserById(id: string): Promise<UserDto> {
    const user = await this.userModel.findById(Types.ObjectId(id)).exec();
    if (!user) {
      throw new NotFoundException(`User with id ${id} does not exist.`);
    }
    return mapUserEntityToUserDto(user);
  }

  public async createUser(userCreateDto: UserCreateDto): Promise<UserDto> {
    const user = await this.userModel.create(userCreateDto);
    return mapUserEntityToUserDto(user);
  }

  public async updateUser(id: string, update: UserPatchDto): Promise<UserDto> {
    const user = await this.userModel.findByIdAndUpdate(Types.ObjectId(id), update).exec();
    if (!user) {
      throw new NotFoundException();
    }
    return mapUserEntityToUserDto(user);
  }

  public async deleteUser(id: string): Promise<UserDto> {
    const user = await this.userModel.findByIdAndDelete(Types.ObjectId(id)).exec();
    if (!user) {
      throw new NotFoundException();
    }
    return mapUserEntityToUserDto(user);
  }

  public async deleteAllUsers(): Promise<number> {
    const res = await this.userModel.deleteMany({}).exec();
    return res.deletedCount;
  }

}
