import { Injectable, NotFoundException } from "@nestjs/common";
import { Model } from "mongoose";
import { Area, User } from "src/data/models";
import { InjectModel } from "@nestjs/mongoose";
import { IdAndNameDto } from "src/data/dtos";
import { mapEntityToIdAndNameDto } from "src/data/mappers";

@Injectable()
export class GeofenceService {

  constructor(
    @InjectModel("Area") private readonly areaModel: Model<Area>,
    @InjectModel("User") private readonly userModel: Model<User>,
  ) {}

  public async getUsersWithinArea(areaName: string): Promise<IdAndNameDto[]> {
    const area = await this.areaModel.findOne({ name: areaName }).exec();
    if (!area) {
      throw new NotFoundException("No area with name " + areaName);
    }
    const users = await this.userModel.find({
      geometry: { $geoWithin: { $geometry: area.geometry } },
    })
    return users.map(mapEntityToIdAndNameDto);
  }

  public async getUsersOutsideArea(areaName: string): Promise<IdAndNameDto[]> {
    const area = await this.areaModel.findOne({ name: areaName }).exec();
    if (!area) {
      throw new NotFoundException("No area with name " + areaName);
    }
    const users = await this.userModel.find({
      geometry: { $not: { $geoWithin: { $geometry: area.geometry} } },
    }).exec();
    return users.map(mapEntityToIdAndNameDto);
  }

  public async getAreasUserIsWithin(userName: string): Promise<IdAndNameDto[]> {
    const user = await this.userModel.findOne({ name: userName }).exec();
    if (!user) {
      throw new NotFoundException("No user with name " + userName);
    }
    const areas = await this.areaModel.find({
      geometry: { $geoIntersects: { $geometry: user.geometry } },
    });
    return areas.map(mapEntityToIdAndNameDto);
  }

}
