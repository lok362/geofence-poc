import { Controller, Get, Param } from "@nestjs/common";
import { GeofenceService } from "./geofence.service";

@Controller("geofence")
export class GeofenceController {

  constructor(
    private readonly geofenceService: GeofenceService,
  ) {}

  @Get("users/within/:areaName")
  public getUsersWithinArea(@Param("areaName") areaName: string) {
    return this.geofenceService.getUsersWithinArea(areaName);
  }

  @Get("users/outside/:areaName")
  public getUsersOutsideArea(@Param("areaName") areaName: string) {
    return this.geofenceService.getUsersOutsideArea(areaName);
  }

  @Get("areas/containing/:userName")
  public getAreasUserIsWithin(@Param("userName") userName: string) {
    return this.geofenceService.getAreasUserIsWithin(userName);
  }

}
