import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { AreaSchema, UserSchema } from "src/data/schemas";
import { GeofenceService } from "./geofence.service";
import { GeofenceController } from "./geofence.controller";

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: "Area", schema: AreaSchema },
      { name: "User", schema: UserSchema },
    ]),
  ],
  controllers: [
    GeofenceController,
  ],
  providers: [
    GeofenceService,
  ],
})
export class GeofenceModule {}
