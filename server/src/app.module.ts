import { Module } from "@nestjs/common";
import { AppController } from "./app.controller";
import { AppService } from "./app.service";
import { MongooseModule } from "@nestjs/mongoose";
import { GeofenceModule } from "./modules/geofence/geofence.module";
import { UserModule } from "./modules/user/user.module";
import { AreaModule } from "./modules/area/area.module";

@Module({
  imports: [
    MongooseModule.forRoot("mongodb://localhost/geofence-poc", {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
    }),
    UserModule,
    AreaModule,
    GeofenceModule,
  ],
  controllers: [
    AppController,
  ],
  providers: [
    AppService,
  ],
})
export class AppModule {}
