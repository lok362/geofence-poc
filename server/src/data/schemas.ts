import { Schema, SchemaDefinition } from "mongoose";

export const AreaSchema = new Schema({
  name: {
    type: String,
    required: true,
    unique: true,
  },
  geometry: {
    type: {
      type: String,
      enum: ["Polygon"],
      required: true,
    },
    coordinates: {
      type: [[[Number]]], // Array of arrays of arrays of numbers
      required: true,
    },
  },
});

export const UserSchema = new Schema({
  name: {
    type: String,
    required: true,
    unique: true,
  },
  geometry: {
    type: {
      type: String,
      enum: ["Point"],
      required: true,
    },
    coordinates: {
      type: [Number],
      required: true,
    },
    required: false,
  },
});