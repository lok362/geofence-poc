import { Document } from "mongoose";

export interface Area extends Document {
  name: string;
  geometry: {
    type: "Polygon";
    coordinates: number[][][];
  };
};

export interface User extends Document {
  name: string;
  geometry: {
    type: "Point";
    coordinates: [number, number];
  };
}