import { User, Area } from "./models";
import { UserDto, AreaDto, IdAndNameDto } from "./dtos";

export const mapUserEntityToUserDto = (user: User): UserDto => ({
  id: user.id,
  name: user.name,
  geometry: user.geometry,
});

export const mapAreaEntityToAreaDto = (area: Area): AreaDto => ({
  id: area.id,
  name: area.name,
  geometry: area.geometry,
});

export const mapEntityToIdAndNameDto = (entity: { id?: string, name: string }): IdAndNameDto => ({
  id: entity.id,
  name: entity.name,
});
