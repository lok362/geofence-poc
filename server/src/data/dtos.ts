
export interface GeoJsonPolygon {
  type: "Polygon",
  coordinates: number[][][];
}

export interface GeoJsonPoint {
  type: "Point";
  coordinates: [number, number];
}

export interface AreaDto {
  id: string;
  name: string;
  geometry: GeoJsonPolygon;
}

export interface AreaCreateDto {
  name: string;
  geometry: GeoJsonPolygon;
}

export interface AreaPatchDto {
  name?: string;
  geometry?: GeoJsonPolygon;
}

export interface UserDto {
  id: string;
  name: string;
  geometry: GeoJsonPoint;
}

export interface UserCreateDto {
  name: string;
  geometry: GeoJsonPoint;
}

export interface UserPatchDto {
  name?: string;
  geometry?: GeoJsonPoint;
}

export interface IdAndNameDto {
  id: string;
  name: string;
}